package com.hamrasta.token.request;



import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.MultiAddress;
import com.hamrasta.token.asset.Tags;
import org.web3j.crypto.Sign;

/**
 * [MultiAddress] [number 4 byte] - [numberX 32 byte] - [nonce 8 byte] - [v 1 byte r 32 byte s 32 byte]
 */
public class TagRequest extends RPCRequest {
    @AssetField(index = 0)
    private MultiAddress address;
    @AssetField(index = 1)
    private Tags tags;

    private TagRequest(MultiAddress address, Tags tags) {

        this.address = address;
        this.tags = tags;
    }

    public TagRequest() {
    }

    public MultiAddress getAddress() {
        return address;
    }

    public void setAddress(MultiAddress address) {
        this.address = address;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public TagRequest(long nonce, MultiAddress address, Tags tags) {
        super(nonce);
        this.address = address;
        this.tags = tags;
    }

    public TagRequest(Sign.SignatureData[] signatures, long nonce, MultiAddress address, Tags tags) {
        super(signatures, nonce);
        this.address = address;
        this.tags = tags;
    }

    public TagRequest(String name, Sign.SignatureData[] signatures, long nonce, MultiAddress address, Tags tags) {
        super(name, signatures, nonce);
        this.address = address;
        this.tags = tags;
    }





}
