package com.hamrasta.token.request;



import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Account;
import com.hamrasta.token.asset.MultiAddress;
import org.web3j.crypto.Sign;

/**
 * [1 byte][32 byte name] [number 4 byte] - [numberX to 25 or 32 byte] - [Permission byte] - [nonce 8 byte] - [v 1 byte r 32 byte s 32 byte]
 */
public class ManageRequest extends RPCRequest {
    @AssetField(index = 0)
    private MultiAddress[] inputs;
    @AssetField(index = 1)
    private Account.Permission permission;

    public ManageRequest() {
    }

    public ManageRequest(MultiAddress[] inputs, Account.Permission permission, long nonce) {
        this("", inputs, permission, nonce);
    }

    public ManageRequest(String name, MultiAddress[] inputs, Account.Permission permission, long nonce) {
        super(nonce);
        this.inputs = inputs;
        this.permission = permission;
        this.name = name;
    }

    public ManageRequest(MultiAddress[] inputs, Account.Permission permission, long nonce,
                         Sign.SignatureData[] signatures) {
        this("", inputs, permission, nonce, signatures);
    }

    public ManageRequest(String name, MultiAddress[] inputs, Account.Permission permission, long nonce,
                         Sign.SignatureData[] signatures) {
        super(signatures, nonce);
        this.inputs = inputs;
        this.permission = permission;
        this.name = name;
    }

    private ManageRequest(MultiAddress[] inputs, Account.Permission permission) {
        this.inputs = inputs;
        this.permission = permission;
    }



    public Account.Permission getPermission() {
        return permission;
    }

    public void setPermission(Account.Permission permission) {
        this.permission = permission;
    }

    public MultiAddress[] getInputs() {
        return inputs;
    }

    public void setInputs(MultiAddress[] inputs) {
        this.inputs = inputs;
    }

}
