package com.hamrasta.token.request;


import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Output;
import org.web3j.crypto.Sign;

/**
 * [1 byte][32 byte name of token][number 4 byte] - [numberX to 25 byte , amount 8 byte] - [nonce 8 byte] - [v 1 byte r 32 byte s 32 byte]
 */
public class MintRequest extends RPCRequest {
    @AssetField(index = 0)
    private Output[] outputs;

    public MintRequest() {
    }

    public MintRequest(Output[] outputs, long nonce) {
        this("", outputs, nonce);
    }

    public MintRequest(String name, Output[] outputs, long nonce) {
        super(nonce);
        this.name = name;
        this.outputs = outputs;

    }

    public MintRequest(Output[] outputs, long nonce, Sign.SignatureData[] signatures) {
        this("", outputs, nonce, signatures);
    }

    public MintRequest(String name, Output[] outputs, long nonce, Sign.SignatureData[] signatures) {
        super(signatures, nonce);
        this.name = name;
        this.outputs = outputs;
    }

    public MintRequest(Output[] outputs) {

        this.outputs = outputs;
    }


    public Output[] getOutputs() {
        return outputs;
    }

    public void setOutputs(Output[] outputs) {
        this.outputs = outputs;
    }

}
