package com.hamrasta.token.request;



import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Threshold;
import org.web3j.crypto.Sign;

/**
 */
public class ThresholdRequest extends RPCRequest {
    @AssetField(index = 0, optional = true, address = true)
    private String address;
    @AssetField(index = 1)
    private Threshold threshold;



    public ThresholdRequest() {
    }

    /**
     *
     * @param address to set default threshold address should be empty
     * @param threshold
     * @param nonce
     */
    public ThresholdRequest(String address, Threshold threshold, long nonce) {
        this("", address, threshold, nonce);
    }
    public ThresholdRequest(String name, String address, Threshold threshold, long nonce) {
        super(nonce);
        this.name = name;
        this.address = address;
        this.threshold = threshold;
    }

    public ThresholdRequest(String name, String address, Threshold threshold, long nonce,
                            Sign.SignatureData[] signatures) {
        super(signatures, nonce);
        this.name = name;
        this.address = address;
        this.threshold = threshold;
    }

    public ThresholdRequest(String address, Threshold threshold, long nonce,
                            Sign.SignatureData[] signatures) {
        this("", address, threshold, nonce, signatures);
    }

    private ThresholdRequest(String address, Threshold threshold) {

        this.address = address;
        this.threshold = threshold;
    }


    public Threshold getThreshold() {
        return threshold;
    }

    public void setThreshold(Threshold threshold) {
        this.threshold = threshold;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
