package com.hamrasta.token.request;



import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Commission;
import org.web3j.crypto.Sign;

/**
 * [1 byte][32 byte name][1 byte 255 if address is present] - [25 byte readableAddress] - [1 byte 255 if address is present] - [25 byte readableAddress] - [Commission]  -
 * [nonce 8 byte] - [v 1 byte r 32 byte s 32 byte]
 */
public class CommissionRequest extends RPCRequest {
    @AssetField(index = 0, optional = true, address = true)
    private String commissionAddress;
    @AssetField(index = 1, optional = true, address = true)
    private String address;
    @AssetField(index = 2)
    private Commission commission;



    public CommissionRequest(String commissionAddress, String address, Commission commission, long nonce) {
        this("", commissionAddress, address, commission, nonce);
    }

    public CommissionRequest(String name, String commissionAddress, String address, Commission commission, long nonce) {
        super(nonce);
        this.name = name;
        this.commissionAddress = commissionAddress;
        this.address = address;
        this.commission = commission;

    }

    public CommissionRequest(String commissionAddress, String address, Commission commission, long nonce,
                             Sign.SignatureData[] signatures) {
        this("", commissionAddress, address, commission, nonce, signatures);
    }

    public CommissionRequest(String name, String commissionAddress, String address, Commission commission, long nonce,
                             Sign.SignatureData[] signatures) {
        super(signatures, nonce);
        this.name = name;
        this.commissionAddress = commissionAddress;
        this.address = address;
        this.commission = commission;

    }

    private CommissionRequest(String commissionAddress, String address, Commission commission) {
        this.commissionAddress = commissionAddress;
        this.address = address;
        this.commission = commission;
    }


    public CommissionRequest() {
    }

    public String getCommissionAddress() {
        return commissionAddress;
    }

    public void setCommissionAddress(String commissionAddress) {
        this.commissionAddress = commissionAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public Commission getCommission() {
        return commission;
    }

    public void setCommission(Commission commission) {
        this.commission = commission;
    }


}
