package com.hamrasta.token.request;


import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import org.web3j.crypto.Sign;

/**
 * [number 4 byte] - [numberX 32 byte has token] - [nonce 8 byte] - [v 1 byte r 32 byte s 32 byte]
 */
public class BurnRequest extends RPCRequest {
    @AssetField(index = 0, length = 32)
    private String[] inputs;


    public BurnRequest() {
    }

    private BurnRequest(String[] inputs) {
        this.inputs = inputs;
    }


    public BurnRequest(String[] inputs, long nonce) {
        super(nonce);
        this.inputs = inputs;
    }

    public String[] getInputs() {
        return inputs;
    }

    public void setInputs(String[] inputs) {
        this.inputs = inputs;
    }

    @Override
    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    @Override
    public Sign.SignatureData[] getSignatures() {
        return signatures;
    }

    public void setSignatures(Sign.SignatureData[] signatures) {
        this.signatures = signatures;
    }

    public BurnRequest(String[] inputs, long nonce, Sign.SignatureData[] signatures) {
        super(signatures, nonce);
        this.inputs = inputs;
    }
    public BurnRequest(String name, String[] inputs, long nonce, Sign.SignatureData[] signatures) {
        super(name, signatures, nonce);
        this.inputs = inputs;
    }







}
