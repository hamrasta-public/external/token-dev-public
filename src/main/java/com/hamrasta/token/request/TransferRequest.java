package com.hamrasta.token.request;


import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Output;
import com.hamrasta.token.asset.Tags;
import org.web3j.crypto.Sign;

/**
 * [1 byte][32 byte name][number 4 byte] - [numberX to 25 byte , amount 8 byte] - [Tags] - [nonce 8 byte]
 * - [number of signatures] - [v 1 byte r 32 byte s 32 byte]
 */
public class TransferRequest extends RPCRequest {
    @AssetField(index = 0)
    private Output[] outputs;
    @AssetField(index = 1)
    private Tags tags; // we can send any token to a tag account so we should have a tags parameter here to set if we want those tokens that have these tags only

    public TransferRequest() {

    }

    public TransferRequest(Output[] outputs, long nonce) {
        super(nonce);
        this.outputs = outputs;
        this.tags = new Tags();
    }

    public TransferRequest(String name, Output[] outputs, long nonce) {
        super(nonce);
        this.outputs = outputs;
        this.tags = new Tags();
        this.name = name;
    }

    public TransferRequest(String name, Output[] outputs, Tags tags, long nonce) {
        super(nonce);
        this.outputs = outputs;
        this.tags = tags == null ? new Tags() : null;
        this.name = name;
    }

    public TransferRequest(String name, Output[] outputs, Tags tags, long nonce, Sign.SignatureData... signatures) {
        super(signatures, nonce);
        this.outputs = outputs;
        this.tags = tags == null ? new Tags() : null;
        this.name = name;
    }

    private TransferRequest(Output[] outputs, Tags tags) {

        this.outputs = outputs;
        this.tags = tags == null ? new Tags() : null;
    }




    public Tags getTags() {
        if (tags == null)
            tags = new Tags();
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public Output[] getOutputs() {
        return outputs;
    }

    public void setOutputs(Output[] outputs) {
        this.outputs = outputs;
    }



}
