package com.hamrasta.token.response;


import com.hamrasta.hplatform.response.RPCResponse;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Account;

public class AccountResponse extends RPCResponse {
    @AssetField(index = 0, address = true)
    private String address;
    @AssetField(index = 1)
    private Account account;

    public AccountResponse() {
    }

    public AccountResponse(String txId, String address, Account account) {
        super(txId);
        this.address = address;
        this.account = account;
    }

    public String getAddress() {
        return address;
    }

    public Account getAccount() {
        return account;
    }

}
