package com.hamrasta.token.response;

import com.hamrasta.hplatform.response.RPCResponse;
import com.hamrasta.hplatform.serializer.AssetField;

public class AccountListResponse extends RPCResponse {
    @AssetField(index = 0)
    private AccountResponse[] accounts;

    public AccountListResponse() {

    }

    public AccountListResponse(String txId, AccountResponse[] accounts) {
        super(txId);
        this.accounts = accounts;
    }

    public AccountResponse[] getAccounts() {
        return accounts;
    }

    public void setAccounts(AccountResponse[] accounts) {
        this.accounts = accounts;
    }

}
