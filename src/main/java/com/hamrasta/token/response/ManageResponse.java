package com.hamrasta.token.response;



import com.hamrasta.hplatform.response.RPCResponse;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Token;

/**
 * [number 4 byte] - [number of AccountResponse] - [number 4 byte] - [Tokens]
 */
public class ManageResponse extends RPCResponse {
    @AssetField(index = 0)
    private AccountResponse[] accounts;
    @AssetField(index = 1)
    private Token[] tokens;

    public ManageResponse() {
    }

    public ManageResponse(String txId, AccountResponse[] accounts, Token[] tokens) {
        super(txId);
        this.accounts = accounts;
        this.tokens = tokens;
    }

    public AccountResponse[] getAccounts() {
        return accounts;
    }

    public void setAccounts(AccountResponse[] accounts) {
        this.accounts = accounts;
    }

    public Token[] getTokens() {
        return tokens;
    }

    public void setTokens(Token[] tokens) {
        this.tokens = tokens;
    }



}
