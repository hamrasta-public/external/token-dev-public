package com.hamrasta.token.response;


import com.hamrasta.token.asset.Account;

public class DecodeResponse {
    private Account account;
    private String address;

    public DecodeResponse() {
    }

    public DecodeResponse(Account account, String address) {
        this.account = account;
        this.address = address;
    }

    public Account getAccount() {
        return account;
    }

    public String getAddress() {
        return address;
    }
}
