package com.hamrasta.token.response;



import com.hamrasta.hplatform.response.RPCResponse;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.token.asset.Token;

/**
 * [number 4 byte] - [Token encoded x number]
 */
public class TokenListResponse extends RPCResponse {
    @AssetField(index = 0)
    private Token[] tokens;

    public TokenListResponse() {
    }

    public TokenListResponse(String txId) {
        super(txId);
    }

    public TokenListResponse(String txId, Token[] tokens) {
        super(txId);
        this.tokens = tokens;
    }

    public Token[] getTokens() {
        return tokens;
    }

    public void setTokens(Token[] tokens) {
        this.tokens = tokens;
    }


}
