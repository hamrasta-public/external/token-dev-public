package com.hamrasta.token.util;


import com.hamrasta.hplatform.asset.Address;
import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.Marshal;
import com.hamrasta.token.asset.Account;
import com.hamrasta.token.response.DecodeResponse;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.web3j.crypto.Sign;

import java.math.BigInteger;
import java.security.SignatureException;
import java.util.LinkedList;
import java.util.List;

public class RequestUtil {

    public static boolean checkSameNetwork(ChaincodeStub stub, String address) {
        boolean testnet = stub.getStringState("token_network_testnet").equals("true");
        return Address.validateAddress(testnet, address);
    }

    public static DecodeResponse decode(ChaincodeStub stub, RPCRequest request) {
        if (request == null) {
            throw new RuntimeException("request is null, bad hex");
        }
        try {
            List<BigInteger> publicKeys = new LinkedList<>();
            for (int i =0; i < request.getSignatures().length; i ++) {
                publicKeys.add(Sign.signedMessageToKey(request.getRawPart(), request.getSignatures()[i]));
            }

            boolean testnet = stub.getStringState("token_network_testnet").equals("true");

            // retrieve address from signature
            String address = Address.computeReadableAddress(testnet, publicKeys.toArray(new BigInteger[0]));

            // check if this address can block
            Account account = decode(stub, address, request.getName());
            if (account == null)
                account = Marshal.unpack(Account.class, stub.getState(address));
            if (account == null)
                throw new RuntimeException("account is null");
            if (account.getNonce() >= request.getNonce())
                throw new RuntimeException("nonce is greater than the request");

            return new DecodeResponse(account, address);
        } catch (SignatureException e) {
            throw new RuntimeException("signature is not valid");
        }

    }
    public static Account decode(ChaincodeStub stub, String address, String groupName) {
        if (groupName != null && !groupName.isEmpty())
            return Marshal.unpack(Account.class, stub.getState(groupName + "_" + address), true);
        else return Marshal.unpack(Account.class, stub.getState(address), true);
    }


}
