package com.hamrasta.token.util;


import com.hamrasta.hplatform.serializer.Marshal;
import com.hamrasta.token.asset.Account;
import com.hamrasta.token.response.AccountResponse;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.Collection;
import java.util.Collections;

public class AccountUtil {
    public static void updateAllAccounts(ChaincodeStub stub, Collection<AccountResponse> accounts, long nonce, String groupName) {
        for (AccountResponse account : accounts) {
            account.getAccount().setNonce(nonce);
            if (groupName == null || groupName.isEmpty())
                stub.putState(account.getAddress(), Marshal.pack(account.getAccount()));
            else
                stub.putState(groupName + "_" + account.getAddress(), Marshal.pack(account.getAccount()));
        }
    }
    public static void updateNonceOfAccount(ChaincodeStub stub, String address, long nonce, String groupName) {
        Account account = Marshal.unpack(Account.class, stub.getState(address));
        if (account != null) {
            updateAllAccounts(stub, Collections.singleton(new AccountResponse(stub.getTxId(), address, account)),
                    nonce, groupName);
        }
    }
}
