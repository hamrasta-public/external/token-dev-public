package com.hamrasta.token.util;


import com.hamrasta.token.asset.Token;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class NewTokenObject {
    private Token token;
    private List<Token> fromTokens = new LinkedList<>();

    public NewTokenObject(Token token, List<Token> fromTokens) {
        this.token = token;
        this.fromTokens = fromTokens;
    }

    public NewTokenObject(Token token, Token... fromTokens) {
        this.token = token;
        this.fromTokens = Arrays.asList(fromTokens);
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public List<Token> getFromTokens() {
        return fromTokens;
    }

    public void setFromTokens(List<Token> fromTokens) {
        this.fromTokens = fromTokens;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewTokenObject that = (NewTokenObject) o;
        return token.equals(that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }
}
