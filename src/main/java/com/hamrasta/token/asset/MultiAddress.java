package com.hamrasta.token.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

/**
 * [byte if token 255 else 0] - [25 or 32 byte for address]
 */
public class MultiAddress implements IAsset {

    @AssetField(index = 2, address = true, ifTrue = "!token")
    private String accountAddress;
    @AssetField(index = 1, length = 32, ifTrue = "token")
    private String tokenHash;
    @AssetField(index = 0)
    private boolean token;

    public MultiAddress() {
    }

    public MultiAddress(String accountAddress, String tokenHash) {
        this.accountAddress = accountAddress;
        this.tokenHash = tokenHash;
        this.token = tokenHash != null && !tokenHash.isEmpty();
    }

    public String getAccountAddress() {
        return accountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        this.accountAddress = accountAddress;
    }

    public String getTokenHash() {
        return tokenHash;
    }

    public void setTokenHash(String tokenHash) {
        this.tokenHash = tokenHash;
    }

    public boolean isToken() {
        return token;
    }

    public void setToken(boolean token) {
        this.token = token;
    }

}
