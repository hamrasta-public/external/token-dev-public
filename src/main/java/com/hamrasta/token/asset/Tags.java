package com.hamrasta.token.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

/**
 *
 * [4 byte number] [number x 32 byte]
 */
public class Tags implements IAsset {

    @AssetField(index = 0, length = 32)
    private String[] tags;


    public Tags() {
    }

    public String[] getTags() {
        if (tags == null)
            tags = new String[0];
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }


    public Tags(String... hex32ByteTags) {
        this.tags = hex32ByteTags;
    }

    public static String getKeyOf(String addressOrHash) {
        return "tag_" + addressOrHash;
    }
    public static String getKeyOf(MultiAddress address) {
        if (address.isToken())
            return "tag_" + address.getTokenHash();
        else
            return "tag_" + address.getAccountAddress();
    }


    @Override
    public IAsset defaultValue() {
        return new Tags();
    }
}
