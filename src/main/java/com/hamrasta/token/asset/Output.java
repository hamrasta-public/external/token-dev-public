package com.hamrasta.token.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

public class Output implements IAsset {
    @AssetField(index = 0, address = true)
    private String to;
    @AssetField(index = 1)
    private long amount;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public Output() {
    }

    public Output(String to, long amount) {
        this.to = to;
        this.amount = amount;
    }
}
