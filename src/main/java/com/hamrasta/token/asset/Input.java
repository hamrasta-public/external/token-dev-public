package com.hamrasta.token.asset;


import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

public class Input implements IAsset {
    @AssetField(index = 0, length = 32)
    private String from;


    public Input() {
    }

    public Input(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
