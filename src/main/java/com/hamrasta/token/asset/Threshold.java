package com.hamrasta.token.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

import java.util.Date;

/**
 * [8 byte min] [8 byte hour max] [8 byte daily max] [8 byte weekly max] [8 byte monthly max] [8 byte annual max]
 */
public class Threshold implements IAsset {

    public static class Sent implements IAsset {
        @AssetField(index = 0)
        private long hourly;
        @AssetField(index = 1)
        private long daily;
        @AssetField(index = 2)
        private long weekly;
        @AssetField(index = 3)
        private long monthly;
        @AssetField(index = 4)
        private long annual;

        public Sent() {
        }

        public Sent(long hourly, long daily, long weekly, long monthly, long annual) {
            this.hourly = hourly;
            this.daily = daily;
            this.weekly = weekly;
            this.monthly = monthly;
            this.annual = annual;
        }

        public long getHourly() {
            return hourly;
        }

        public void setHourly(long hourly) {
            this.hourly = hourly;
        }

        public long getDaily() {
            return daily;
        }

        public void setDaily(long daily) {
            this.daily = daily;
        }

        public long getWeekly() {
            return weekly;
        }

        public void setWeekly(long weekly) {
            this.weekly = weekly;
        }

        public long getMonthly() {
            return monthly;
        }

        public void setMonthly(long monthly) {
            this.monthly = monthly;
        }

        public long getAnnual() {
            return annual;
        }

        public void setAnnual(long annual) {
            this.annual = annual;
        }

    }

    public static final String THRESHOLD_KEY = "token_threshold_";

    @AssetField(index = 0)
    private long min;
    @AssetField(index = 1)
    private Sent sent;

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public Sent getSent() {
        return sent;
    }

    public void setSent(Sent sent) {
        this.sent = sent;
    }

    public Threshold(long min, Sent sent) {
        this.min = min;
        this.sent = sent;
    }

    public Threshold() {
    }


    public static String getSentKeyHourly(String readableAddress, String groupName) {
        return groupName + "token_sent_hour_" + (int)(Math.floor(new Date().getTime() / (1000.0 * 3600))) + "_" + readableAddress;
    }
    public static String getSentKeyDaily(String readableAddress, String groupName) {
        return groupName + "token_sent_day_" + (int)(Math.floor(new Date().getTime() / (1000.0 * 3600 * 24))) + "_" + readableAddress;
    }
    public static String getSentKeyWeekly(String readableAddress, String groupName) {
        return groupName + "token_sent_week_" + (int)(Math.floor(new Date().getTime() / (1000.0 * 3600 * 24 * 7))) + "_" + readableAddress;
    }
    public static String getSentKeyMonthly(String readableAddress, String groupName) {
        return groupName + "token_sent_month_" + (int)(Math.floor(new Date().getTime() / (1000.0 * 3600 * 24 * 30))) + "_" + readableAddress;
    }
    public static String getSentKeyAnnual(String readableAddress, String groupName) {
        return groupName + "token_sent_year_" + (int)(Math.floor(new Date().getTime() / (1000.0 * 3600 * 24 * 365))) + "_" + readableAddress;
    }
}
