package com.hamrasta.token.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;
import org.apache.commons.codec.binary.Hex;
import org.web3j.crypto.Hash;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Objects;

/**
 * [hash 32 byte] - [owner 25 byte] - [optional changedFrom 1 byte] - [changedFrom
 * hash 32 byte] - [amount - 8 byte]
 * - [1 byte freeze = 4, burn = 2, spent = 1] - [nonce 8 byte]
 */

public class Token implements IAsset {


    private final static byte freezeByte = 0x4;
    private final static byte burnByte = 0x2;
    private final static byte spentByte = 0x1;

    private boolean spent = false;
    private boolean burn = false;

    @AssetField(index = 0, length = 32)
    private String hash;

    @AssetField(index = 1, address = true)
    private String owner;

    @AssetField(index = 3)
    private long amount;

    @AssetField(index = 5)
    private long nonce;

    @AssetField(index = 2, length = 32, optional = true)
    private String changedFrom = "";

    @AssetField(index = 4, customEncoder = "encode", customDecoder = "decode")
    private boolean freeze = false;

    public boolean isBurn() {
        return burn;
    }

    public void setBurn(boolean burn) {
        this.burn = burn;
    }

    public boolean isSpent() {
        return spent;
    }

    public void setSpent(boolean spent) {
        this.spent = spent;
    }

    public boolean isFreeze() {
        return freeze;
    }

    public void setFreeze(boolean freeze) {
        this.freeze = freeze;
    }

    public String getOwner() {
        return owner;
    }

    private void setOwner(String owner) {
        this.owner = owner;
    }

    public long getAmount() {
        return amount;
    }

    public void burn() {
        setBurn(true);
    }

    private void setAmount(long amount) {
        this.amount = amount;
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    public String getChangedFrom() {
        return changedFrom;
    }

    private void setChangedFrom(String changedFrom) {
        if (changedFrom == null)
            changedFrom = "";
        this.changedFrom = changedFrom;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void encode(ByteArrayOutputStream bos) {
        bos.write((freeze ? freezeByte : 0) | (burn ? burnByte : 0) | (spent ? spentByte : 0));
    }

    public Object decode(ByteArrayInputStream bis) {
        byte status = (byte) bis.read();
        boolean spent = (status & spentByte) == spentByte;
        boolean burn = (status & burnByte) == burnByte;
        boolean freeze = (status & freezeByte) == freezeByte;
        setBurn(burn);
        setFreeze(freeze);
        setSpent(spent);
        return freeze;
    }



    public Token() {

    }

    private Token(boolean spent, boolean burn, String hash, String owner, long amount, long nonce, String changedFrom
            , boolean freeze) {
        this.burn = burn;
        this.spent = spent;
        this.hash = hash;
        this.owner = owner;
        this.amount = amount;
        this.nonce = nonce;
        this.changedFrom = changedFrom;
        this.freeze = freeze;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return hash.equals(token.hash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash);
    }

    public boolean payable() {
        return !freeze && !spent && !burn;
    }


    public static class Builder {
        private Token token;

        public Builder() {
            token = new Token();
        }

        public Builder setOwner(String readableAddress) {
            token.setOwner(readableAddress);
            return this;
        }

        public Builder setAmount(long amount) {
            token.setAmount(amount);
            return this;
        }

        public Builder setNonce(long nonce) {
            token.setNonce(nonce);
            return this;
        }

        public Builder setChangedFrom(String tokenHash) {
            token.setChangedFrom(tokenHash);
            return this;
        }

        public Token build(String requestAddress) {
            byte[] data = (requestAddress + token.owner + token.amount + token.nonce + token.changedFrom).getBytes();
            token.setHash(Hex.encodeHexString(Hash.sha256(Hash.sha256(data))));
            return token;
        }

    }

}
