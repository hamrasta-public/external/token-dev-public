package com.hamrasta.token.asset;


import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Objects;


/**
 * Account is only 1 byte
 * The other 8 byte is for nonce
 * bit array
 * [burner][minter][blocker][canBurn][canMint][canBlock][canReceive][canSend]---[8 byte nonce]
 */

public class Account implements IAsset {
    @AssetField(index = 0)
    private Permission permission;

    @AssetField(index = 1)
    private long nonce;

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    public Account() {
    }

    public Account(Permission permission, long nonce) {
        this.permission = permission;
        this.nonce = nonce;
    }

    @Override
    public IAsset defaultValue() {
        return new Account(Permission.defaultPermission(), 0);
    }

    public static class Permission implements IAsset {
        public static final byte SEND = (byte) 0x1;
        public static final byte RECEIVE = (byte) 0x2;
        public static final byte BLOCK = (byte) 0x4;
        public static final byte MINT = (byte) 0x8;
        public static final byte BURN = (byte) 0x16;
        public static final byte BLOCKER = (byte) 0x32;
        public static final byte MINTER = (byte) 0x64;
        public static final byte BURNER = (byte) 0x80;

        @AssetField(index = 0, customEncoder = "encode", customDecoder = "decode")
        private boolean canSend = false;
        private boolean canReceive = true;
        private boolean canBlock = false;
        private boolean canMint = false;
        private boolean canBurn = false;
        private boolean blocker = false;
        private boolean minter = false;
        private boolean burner = false;



        public static void canFreeze(Permission permission) {
            if (!permission.getCanBlock())
                throw new RuntimeException("This account can not freeze or unfreeze token");

        }

        public static boolean check(Permission accountPermission, Permission requestedPermission, Permission anotherAccountPermission) {
            if (!(anotherAccountPermission.getCanSend() & requestedPermission.getCanSend()) && !accountPermission.getCanBlock())
                throw new RuntimeException("This account can not block another account");
            else if (!(anotherAccountPermission.getCanReceive() & requestedPermission.getCanReceive()) && !accountPermission.getCanBlock())
                throw new RuntimeException("This account can not unblock another account");
            else if (!(anotherAccountPermission.getCanBlock() & requestedPermission.getCanBlock()) && !accountPermission.isBlocker())
                throw new RuntimeException("This account does not have blocker permission");
            else if (!(anotherAccountPermission.getCanBurn() & requestedPermission.getCanBurn()) && !accountPermission.isBurner())
                throw new RuntimeException("This account does not have burner permission");
            else if (!(anotherAccountPermission.getCanMint() & requestedPermission.getCanMint()) && !accountPermission.isMinter())
                throw new RuntimeException("This account does not have minter permission");
            return true;
        }



        public boolean getCanSend() {
            return canSend;
        }

        public boolean getCanReceive() {
            return canReceive;
        }

        public boolean getCanBlock() {
            return canBlock;
        }

        public boolean getCanMint() {
            return canMint;
        }

        public boolean getCanBurn() {
            return canBurn;
        }

        public void setCanSend(boolean canSend) {
            this.canSend = canSend;
        }

        public void setCanReceive(boolean canReceive) {
            this.canReceive = canReceive;
        }

        public void setCanBlock(boolean canBlock) {
            this.canBlock = canBlock;
        }

        public void setCanMint(boolean canMint) {
            this.canMint = canMint;
        }

        public void setCanBurn(boolean canBurn) {
            this.canBurn = canBurn;
        }

        public boolean isBlocker() {
            return blocker;
        }

        public void setBlocker(boolean blocker) {
            this.blocker = blocker;
        }

        public boolean isMinter() {
            return minter;
        }

        public void setMinter(boolean minter) {
            this.minter = minter;
        }

        public boolean isBurner() {
            return burner;
        }

        public void setBurner(boolean burner) {
            this.burner = burner;
        }

        public Permission() {

        }

        public Permission(boolean canSend, boolean canReceive, boolean canBlock, boolean canMint, boolean canBurn,
                          boolean blocker, boolean minter, boolean burner) {
            this.canSend = canSend;
            this.canReceive = canReceive;
            this.canBlock = canBlock;
            this.canMint = canMint;
            this.canBurn = canBurn;
            this.blocker = blocker;
            this.minter = minter;
            this.burner = burner;
        }
        public void encode(ByteArrayOutputStream bos) {
            bos.write(encode());
        }
        public Object decode(ByteArrayInputStream bis) {
            Permission p = decode((byte)bis.read());
            setBlocker(p.blocker);
            setBurner(p.burner);
            setCanBlock(p.canBlock);
            setCanBurn(p.canBurn);
            setCanMint(p.canMint);
            setCanReceive(p.canReceive);
            setCanSend(p.canSend);
            setMinter(p.minter);
            return p.canSend;
        }
        private byte encode() {
            return encode(canSend, canReceive, canBlock, canMint, canBurn, blocker, minter, burner);
        }

        public static byte encode(boolean canSend, boolean canReceive, boolean canBlock, boolean canMint,
                                  boolean canBurn, boolean blocker, boolean minter, boolean burner) {
            byte canSendByte = canSend ? (byte) Permission.SEND : (byte) 0x0;
            byte canReceiveByte = canReceive ? (byte) Permission.RECEIVE : (byte) 0x0;
            byte canBlockByte = canBlock ? (byte) Permission.BLOCK : (byte) 0x0;
            byte canMintByte = canMint ? (byte) Permission.MINT : (byte) 0x0;
            byte canBurnByte = canBurn ? (byte) Permission.BURN : (byte) 0x0;
            byte blockerByte = blocker ? (byte) Permission.BLOCKER : (byte) 0x0;
            byte minterByte = minter ? (byte) Permission.MINTER : (byte) 0x0;
            byte burnerByte = burner ? (byte) Permission.BURNER : (byte) 0x0;
            return (byte) (canSendByte | canReceiveByte | canBlockByte | canMintByte | canBurnByte | blockerByte | minterByte | burnerByte);
        }

        private Permission decode(byte data) {
            return new Permission((data & Permission.SEND) == Permission.SEND,
                    (data & Permission.RECEIVE) == Permission.RECEIVE,
                    (data & Permission.BLOCK) == Permission.BLOCK,
                    (data & Permission.MINT) == Permission.MINT,
                    (data & Permission.BURN) == Permission.BURN,
                    (data & Permission.BLOCKER) == Permission.BLOCKER,
                    (data & Permission.MINTER) == Permission.MINTER,
                    (data & Permission.BURNER) == Permission.BURNER
                    );
        }

        public static Permission defaultPermission() {
            return new Permission(false, true, false, false, false, false, false, false);
        }

        public static Permission kycPermission() {
            return new Permission(true, true, false, false, false, false, false, false);
        }

        public static Permission superAdminPermission() {
            return new Permission(true, true, true, true, true, true, true, true);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Permission that = (Permission) o;
            return encode() == that.encode();
        }

        @Override
        public int hashCode() {
            return Objects.hash(encode());
        }
    }
}
