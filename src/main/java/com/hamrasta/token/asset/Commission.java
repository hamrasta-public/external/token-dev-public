package com.hamrasta.token.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * [8 byte decimal point] [4 byte floating point if it is percent or zero if it is fixed] [8 byte min value if it is percent] [8 byte max value if it is percent]
 */
public class Commission implements IAsset {

    public static final String COMMISSION_ADDRESS = "token_commission_address";
    public static final String COMMISSION_RATE = "token_commission_rate_";

    @AssetField(index = 0)
    private long decimal;
    @AssetField(index = 1)
    private int floating;
    @AssetField(index = 2, ifTrue = "floating")
    private long min;
    @AssetField(index = 3, ifTrue = "floating")
    private long max;

    public Commission() {
    }

    public boolean isPercent() {
        return floating > 0;
    }

    public BigDecimal getPercent() {
        if (isPercent()) {
            return BigDecimal.valueOf(decimal)
                    .setScale(4, RoundingMode.HALF_DOWN)
                    .divide(BigDecimal.valueOf(floating), RoundingMode.HALF_DOWN).setScale(4, RoundingMode.HALF_DOWN);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public long getDecimal() {
        if (decimal < 0)
            decimal = 0;
        return decimal;
    }

    public void setDecimal(long decimal) {
        this.decimal = decimal;
    }

    public int getFloating() {
        if (floating < 0)
            floating = 0;
        return floating;
    }

    public void setFloating(int floating) {
        this.floating = floating;
    }

    public long getMin() {
        if (floating <= 0)
            min = 0;
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getMax() {
        if (floating <= 0)
            max = Long.MAX_VALUE;
        if (max <= 0)
            max = Long.MAX_VALUE;
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    /**
     * If your floating is greater than zero decimal/floating is a percent commission.
     * if floating is zero then min and max is going to be zero
     * if max is zero then max is Long.MAX_VALUE
     * @param decimal
     * @param floating
     * @param min
     * @param max
     */
    public Commission(long decimal, int floating, long min, long max) {
        this.decimal = decimal;
        this.floating = floating;
        this.min = min;
        this.max = max;
    }

}
