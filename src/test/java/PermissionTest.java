
import com.hamrasta.hplatform.serializer.Marshal;
import com.hamrasta.token.asset.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class PermissionTest {
    @Test
    public void test() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        byte permissionByte = Account.Permission.encode(true, true, true, true, true, true, true, true);
        Assertions.assertEquals(permissionByte, (byte) 255);
        Account.Permission superAdmin = Marshal.unpack(Account.Permission.class, permissionByte);
        Account.Permission p1 = Account.Permission.defaultPermission();

        Assertions.assertTrue(Account.Permission.check(superAdmin,
                Marshal.unpack(Account.Permission.class, Account.Permission.encode(true, true, true, false, false, false, false,
                        false)), p1));
        Assertions.assertThrows(RuntimeException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                Account.Permission.check(p1, superAdmin, p1);
            }
        });
    }

}
