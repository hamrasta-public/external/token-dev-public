
import com.hamrasta.hplatform.serializer.Marshal;
import com.hamrasta.token.asset.Commission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CommissionTest {
    @Test
    public void commission() {
        Commission commission = new Commission(1, 0 ,0 ,0 );
        Commission commission2 = Marshal.unpack(Commission.class, Marshal.pack(commission));
        Assertions.assertFalse(commission.isPercent());
        Assertions.assertNotNull(commission2);
        Assertions.assertEquals(commission.getDecimal(), commission2.getDecimal());
        commission = new Commission(1, 100, 10, 0);
        Assertions.assertEquals(0.01, commission.getPercent().doubleValue());
        Assertions.assertEquals(commission.getMax(), Long.MAX_VALUE);
    }


}
