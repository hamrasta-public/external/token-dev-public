import com.hamrasta.hplatform.asset.Address;
import com.hamrasta.hplatform.serializer.Marshal;
import com.hamrasta.token.asset.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Date;

public class TokenTest {
    @Test
    public void token() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        Address address = Address.generate(true);
        long nonce = new Date().getTime();
        Token token = new Token.Builder().setOwner(address.getReadableAddress())
        .setAmount(100).setNonce(nonce).setChangedFrom("").build(address.getReadableAddress());
        Token token2 = Marshal.unpack(Token.class, Marshal.pack(token));
        Assertions.assertEquals(token, token2);
    }

    @Test
    public void create() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        Address address = Address.generate(false);
    }

}
